String basePath = 'JMP_task8'
String gitRepo = 'https://Alena_Grouk@bitbucket.org/Alena_Grouk/jmp_task6_services.git'
String gitCredentials = '2c244f6c-573b-4be2-9b3e-2f76ee2c8fbb'
String repoOwner = 'Alena_Grouk'
String repoName = 'jmp_task6_services'
String remoteBranch = 'origin'
String deployUrl = 'http://localhost:8080/'
String tomcatUser = 'deployer'

folder("$basePath")

job("$basePath/services_job") {
    scm {
        git {
            // Adds a remote.
            remote {
                // Sets the remote URL.
                url(gitRepo)
                // Credentials used to scan branches and check out sources.
                credentials(gitCredentials)
            }

            // Adds additional behaviors.
            extensions {
                // Allows to perform a merge to a particular branch before building.
                mergeOptions {
                    // Sets the name of the branch to merge.
                    branch('${targetBranch}')
                    // Sets the name of the repository that contains the branch to merge.
                    remote(remoteBranch)
                }
            }
        }
    }

    triggers {
        bitbucketBuildTrigger {
            projectPath(gitRepo)
            cron('* * * * *')
            credentialsId(gitCredentials)
            username(repoOwner)
            password('')
            repositoryOwner(repoOwner)
            repositoryName(repoName)
            // The identifier needs to be unique among your Jenkins jobs related to this repo.
            ciKey('jenkins')
            // This value is the name of the current job when showing build statuses for a pull request.
            ciName('Jenkins')
            checkDestinationCommit(true)
            // Filter option in custom format.
            branchesFilter('')
            branchesFilterBySCMIncludes(false)
            ciSkipPhrases('')
            approveIfSuccess(true)
            cancelOutdatedJobs(false)
            commentTrigger('test this please')
        }
    }

    steps {
        // Invokes a Gradle build script.
        gradle {
            // Specifies the name of the Gradle installation to use if not using the wrapper.
            gradleName('gradle')
            // Use the Gradle wrapper to invoke the build script.
            useWrapper(false)
            // Specifies the Gradle tasks to be invoked.
            tasks('clean build')
        }
    }

    publishers {
        deployPublisher {
            adapters {
                tomcat7xAdapter {
                    url(deployUrl)
                    password(tomcatUser)
                    userName(tomcatUser)
                }
            }
            // War/ear files to deploy.
            war('**/*.war')
            onFailure(false)
            contextPath('')
        }
    }
}